=== Ninja Forms Slack Integration ===
Contributors: NikV
Tags: Ninja Forms, Slack
Requires at least: 4.0
Tested up to: 4.1
Stable tag: 1.0
License: GNU GPLv2+
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Send notifications of new Form Submissions (with user information if logged in) to a Slack channel.

== Description ==
A simple plugin that allows you to get Slack notifications every time somebody submit a form through Ninja Forms.

== Installation ==

1. Unpack the entire contents of this plugin zip file into your `wp-content/plugins/` folder locally
2. Upload to your site
3. Navigate to `wp-admin/plugins.php` on your site (your WP Admin plugin page)
4. Activate this plugin

OR you can just install it with WordPress by going to Plugins >> Add New >> and type this plugin\'s name

== Frequently Asked Questions ==
= Setup A New Slack WebHook =
1. Go To `https://.slack.com/services/new/incoming-webhook`
2. Create a new webhook
3. Set a channel to receive the notifications
4. Copy the URL for the webhook

= Setting Up The Plugin =
1. When creating/updating a Ninja Form, You can go into the Emails & Actions tab to create a new Slack notification. This means you can send multiple notifications of one tab into many channels.

== Screenshots ==

== Changelog ==
= 1.0 =
Version 1.0 Release

== Upgrade Notice ==
= 1.0 =
Initial Commit!